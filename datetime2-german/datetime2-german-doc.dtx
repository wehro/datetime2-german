% \iffalse
%% Doc-Source file to use with LaTeX2e
%% Copyright (C) 2015 Nicola Talbot, all rights reserved.
%% Copyright (C) 2017-19 Sebastian Friedl, all rights reserved.
%%
%%  This work is subject to the LaTeX Project Public License, Version 1.3c or -- at
%%  your option -- any later version of this license.
%%  
%%
%%  This work has the LPPL maintenance status 'maintained'.
%%  Current maintainer of the work is Sebastian Friedl.
% \fi

% !TeX spellcheck=en_US

\documentclass[11pt]{ltxdoc}

\RequirePackage{iftex}
\RequireLuaTeX


\usepackage[no-math]{fontspec}
\usepackage{polyglossia}
\setdefaultlanguage{english}
\usepackage[english]{selnolig}

\usepackage{alltt}
\usepackage{csquotes}
\usepackage{graphicx}
\usepackage{hologo}
\usepackage{multicol}

\usepackage{array}
\usepackage{booktabs}
\usepackage{tabularx}

\usepackage[unicode, pdfborder={0 0 0}, linktoc=all, hyperindex=false]{hyperref}
\usepackage{verbatim}

\usepackage[erewhon]{newtxmath}
\setmainfont{Source Serif Pro}
\setsansfont{Source Sans Pro}
\setmonofont[Scale=MatchLowercase]{Hack}

\usepackage[a4paper, left=4.50cm,right=2.75cm,top=3.25cm,bottom=2.75cm,nohead]{geometry}

\parindent 0pt

\CheckSum{1068}

\renewcommand*{\usage}[1]{\hyperpage{#1}}
\renewcommand*{\main}[1]{\hyperpage{#1}}
\IndexPrologue{\section*{\indexname}\markboth{\indexname}{\indexname}}
\setcounter{IndexColumns}{2}

\newcommand*{\sty}[1]{\textsf{#1}}
\newcommand*{\opt}[1]{\texttt{#1}\index{#1=\texttt{#1}|main}}

\RecordChanges
\PageIndex
\CodelineNumbered


\MakeShortVerb{"}

\title{German Module for the \sty{datetime2} Package \\ {\large\url{https://gitlab.com/SFr682k/datetime2-german}}}
\author{%
  \begin{tabularx}{.8\textwidth}{*{2}{>{\centering\arraybackslash}X}}
    Nicola L. C. Talbot & Sebastian Friedl \\
    (inactive)          & {\normalsize\href{mailto:sfr682k@t-online.de}{\texttt{sfr682k@t-online.de}}} \\
  \end{tabularx}%
}
\date{2019/12/13 (v3.0)}

\hypersetup{pdftitle={datetime2.sty German Module}, pdfauthor={Nicola L.C. Talbot, Sebastian Friedl}}


\begin{document}
    \maketitle
    
    
    \begin{abstract}
         This is the German language module for the \sty{datetime2} package. It defines a regionless style as well as variant styles ("de-DE", "de-AT" and "de-CH").
         
         If you want to use the settings in this module you must install it in addition to installing \sty{datetime2}.
         If you use \sty{babel} or \sty{polyglossia}, you will need this module to prevent them from redefining \cs{today}.
         
         The \sty{datetime2} \opt{useregional} setting must be set to "text" or "numeric" for the language styles to be set.
         Alternatively, you can set the style in the document using \cs{DTMsetstyle}, but this may be changed by \cs{date}\meta{language} depending on the value of the \opt{useregional} setting.
    \end{abstract}




    \clearpage
    \tableofcontents
    \clearpage
    
    
    
    \subsection*{Installation}
    \addcontentsline{toc}{subsection}{Installation}
    Extract the language definition files first:
    \begin{enumerate}
        \item Run \LaTeX\ over the file "datetime2-german.ins": \\
            "latex datetime2-german.ins"
            
        \item Move all "*.ldf" files to "TEXMF/tex/latex/datetime2-contrib/datetime2-german/"
    \end{enumerate}
    Then, you can compile the documentation yourself by executing \\[\smallskipamount]
    "lualatex datetime2-german-doc.dtx" \\
    "makeindex -s gind.ist datetime2-german-doc.idx" \\
    "makeindex -s gglo.ist -o datetime2-german-doc.gls datetime2-german-doc.glo" \\
    "lualatex datetime2-german-doc.dtx" \\
    "lualatex datetime2-german-doc.dtx" \\[\medskipamount]
    or just use the precompiled documentation shipped with the source files. \\
    In both cases, copy the files "datetime2-german-doc.pdf" and "README.md" to \\
    "TEXMF/doc/latex/datetime2-contrib/datetime2-german/"
    
    
    \subsection*{License}
    \addcontentsline{toc}{subsection}{License}
    This material is subject to the \LaTeX\ Project Public License, Version 1.3c or later. \\
    Details may be found in the respective language definition file's copyright header.
    
    
    \subsection*{Acknowledgments}
    \addcontentsline{toc}{subsection}{Acknowledgments}
    Thanks to \dots
    \begin{itemize}
        \item Jürgen Spitzmüller for his valuable advice while preparing version 2.0 of this module.
        
        \item Bernhard Waldbrunner for his merge request, which resulted in the adoption of the DD.MM.YYYY numeric date format with the Austrian numeric style in version 3.0.
        Previous versions used the ISO format (YYYY-MM-DD) as recommended by ÖNORM~1080, which was withdrawn in May~2018.
    \end{itemize}
    
    
    
    \clearpage
    \part{The Documentation}
    \section{Usage}
    
    
    \subsection{Setting up \sty{datetime2} with a language module}
    \textit{There are several methods of loading a language module. \\ Please refer to \sty{datetime2}'s documentation for details.}
    
    \bigskip
    \textbf{Variant 1:} \\
    Set the date style explicitly by passing "german", "de-DE", "de-AT" or "de-CH" to \sty{datetime2}: \\[\smallskipamount]
    "\documentclass{article}" \\
    "\usepackage[german]{datetime2}" \\
    "\begin{document}" \\
    "\today" \\
    "\end{document}"

    \medskip
    \textbf{Variant 2: Pick up the desired style from \sty{babel} or document class options)} \\
    Pass the language (e.g. "german", "ngerman", "austrian", "naustrian", \dots) as an option to the \cs{documentclass} command (or \sty{babel} itself). As soon as the \opt{useregional} option is passed to \sty{datetime2}, the suitable language module is loaded: \\[\smallskipamount]
    "\documentclass[german]{article}" \\
    "\usepackage{babel}" \\
    "\usepackage[useregional]{datetime2}" \\
    "\begin{document}" \\
    "\today" \\
    "\end{document}"
    
    
    
    
    
    \subsection{Customization}
    There are a number of settings provided that can be used in \cs{DTMlangsetup} to modify the date-time style. These are:
    \begin{description}
    	\item["dowdaysep"]%
    	The separator between the day of week name and the day of month number.
    
    	\item["daymonthsep"]%
    	The separator between the day and the month name.
    
    	\item["monthyearsep"]%
    	The separator between the month name and year.
    
    	\item["datesep"]%
    	The separator between the date numbers in the "numeric" styles.
    
    	\item["timesep"]%
    	The separator between hours, minutes and seconds.
    
    	\item["datetimesep"]%
    	The separator between the date and time for the full date-time format.
    
    	\item["timezonesep"]%
    	The separator between the time and zone for the full date-time format.
    
    	\item["abbr"]%
    	This is a boolean key. If "true", the month (and weekday name, if shown) is abbreviated.
    
    	\item["mapzone"]%
    	This is a boolean key. If "true", the time zone mappings are applied.
    
    	\item["showdayofmonth"]%
    	A boolean key that determines whether or not to show the day of the month.
    
    	\item["showyear"]%
    	A boolean key that determines whether or not to show the year.
    
    	\item["twodigits"]%
    	A Boolean key that determines whether or not to use two-digit days and months in the "numeric" styles.
    \end{description}

    Although these keys are \textit{defined} for all variant styles, it depends on \sty{datetime2}'s configuration and the requested styles whether they're \textit{used}. More details about the \cs{DTMlangsetup} command can be found in \sty{datetime2}'s documentation.


    
    
    % TODO: check styles
    \section{The Modules}
    \subsection{Regionless style (\texttt{german})}
    \begin{tabularx}{\textwidth}{>{\itshape}rX}
    	\toprule
    	  \upshape\textbf{Textual style} & 13. Dezember 2019, 12:42:00 MEZ          \\
    	       with \opt{showdow} option & Freitag, 13. Dezember 2019, 12:42:00 MEZ \\
    	                           abbr. & 13. Dez. 2019, 12:42:00 MEZ              \\
    	abbr., with \opt{showdow} option & Fr, 13. Dez. 2019, 12:42:00 MEZ          \\ \midrule
    	  \upshape\textbf{Numeric style} & 13.12.2019, 12:42:00 MEZ                 \\
    	       with \opt{showdow} option & Freitag, 13.12.2019, 12:42:00 MEZ        \\
    	                           abbr. & 13.12.19, 12:42:00 MEZ                   \\
    	abbr., with \opt{showdow} option & Fr, 13.12.19, 12:42:00 MEZ               \\ \bottomrule
    \end{tabularx}


    \subsection{German style (\texttt{de-DE})}
    \begin{tabularx}{\textwidth}{>{\itshape}rX}
    	\toprule
    	  \upshape\textbf{Textual style} & 13. Dezember 2019, 12:42:00 MEZ          \\
    	       with \opt{showdow} option & Freitag, 13. Dezember 2019, 12:42:00 MEZ \\
    	                           abbr. & 13. Dez. 2019, 12:42:00 MEZ              \\
    	abbr., with \opt{showdow} option & Fr, 13. Dez. 2019, 12:42:00 MEZ          \\ \midrule
    	  \upshape\textbf{Numeric style} & 13.12.2019, 12:42:00 MEZ                 \\
    	       with \opt{showdow} option & Freitag, 13.12.2019, 12:42:00 MEZ        \\
    	                           abbr. & 13.12.19, 12:42:00 MEZ                   \\
    	abbr., with \opt{showdow} option & Fr, 13.12.19, 12:42:00 MEZ               \\ \bottomrule
    \end{tabularx}


    \subsection{Austrian style (\texttt{de-AT})}
    \begin{tabularx}{\textwidth}{>{\itshape}rX}
    	\toprule
    	  \upshape\textbf{Textual style} & 13. Dezember 2019, 12:42:00 MEZ          \\
    	       with \opt{showdow} option & Freitag, 13. Dezember 2019, 12:42:00 MEZ \\
    	                           abbr. & 13. Dez. 2019, 12:42:00 MEZ              \\
    	abbr., with \opt{showdow} option & Fr, 13. Dez. 2019, 12:42:00 MEZ          \\ \midrule
    	  \upshape\textbf{Numeric style} & 13.12.2019, 12:42:00 MEZ                 \\
    	       with \opt{showdow} option & Freitag, 13.12.2019, 12:42:00 MEZ        \\
    	                           abbr. & 13.12.19, 12:42:00 MEZ                   \\
    	abbr., with \opt{showdow} option & Fr, 13.12.19, 12:42:00 MEZ               \\ \bottomrule
    \end{tabularx}


    \subsection{Swiss style (\texttt{de-CH})}
    \begin{tabularx}{\textwidth}{>{\itshape}rX}
    	\toprule
    	  \upshape\textbf{Textual style} & 13. Dezember 2019, 12.42.00 Uhr MEZ          \\
    	       with \opt{showdow} option & Freitag, 13. Dezember 2019, 12.42.00 Uhr MEZ \\
    	                           abbr. & 13. Dez. 2019, 12.42.00 Uhr MEZ              \\
    	abbr., with \opt{showdow} option & Fr, 13. Dez. 2019, 12.42.00 Uhr MEZ          \\ \midrule
    	  \upshape\textbf{Numeric style} & 13.12.2019, 12.42.00 Uhr MEZ                 \\
    	       with \opt{showdow} option & Freitag, 13.12.2019, 12.42.00 Uhr MEZ        \\
    	                           abbr. & 13.12.19, 12.42.00 Uhr MEZ                   \\
    	abbr., with \opt{showdow} option & Fr, 13.12.19, 12.42.00 Uhr MEZ               \\ \bottomrule
    \end{tabularx}
    
    
    


    \StopEventually{%
        \clearpage
        \phantomsection
        \addcontentsline{toc}{section}{Change History}%
        \PrintChanges%
        \addcontentsline{toc}{section}{\indexname}%
        \PrintIndex%
    }





    \clearpage
    \part{The Code}
    % TODO: Load code modules
    
    \section{The Base Module}
    The "german-base" module provides code common to all regional variations and engine-dependent code, e.\,g. commands printing weekday and month names.
    
    \DocInput{datetime2-german-base.dtx}
    \DocInput{datetime2-german-base-ascii.dtx}
    \DocInput{datetime2-german-base-utf8.dtx}
    

    \section{Regionless Style: \texttt{datetime2-german.ldf}}
    \DocInput{datetime2-german.dtx}

    \section{Regional Variations}
    \DocInput{datetime2-de-DE.dtx}
    \DocInput{datetime2-de-AT.dtx}
    \DocInput{datetime2-de-CH.dtx}

    \Finale
\end{document}
